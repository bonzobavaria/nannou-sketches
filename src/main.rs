mod virtual_keyboard;

use nannou::prelude::*;
//use rand::prelude::*;

fn main() {
    nannou::app(model).update(update).run()
}

struct Ball {
    x_y: (f32, f32),
    size: f32,
    velocity: (f32, f32),
}

impl Ball {
    pub fn new() -> Ball {
        Ball {
            x_y: (
                rand::random::<f32>() * 600.0 - 300.0,
                rand::random::<f32>() * 600.0 - 300.0,
            ),
            size: rand::random::<f32>() * 50.0,
            velocity: (rand::random::<f32>() - 0.5, rand::random::<f32>() - 0.5),
        }
    }
}

struct Model {
    balls: Vec<Ball>,
}

fn model(app: &App) -> Model {
    let mut balls: Vec<Ball> = vec![];
    for _ in 0..1000 {
        balls.push(Ball::new());
    }
    app.new_window()
        .with_dimensions(600, 600)
        .view(view)
        .build()
        .unwrap();
    Model { balls }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    for ball in model.balls.iter_mut() {
        let (x, y) = ball.x_y;
        let (mut x_vel, mut y_vel) = ball.velocity;
        if x >= 299.0 || x <= -299.0 {
            x_vel *= -1.0;
            ball.velocity = (x_vel, y_vel);
        }
        if y >= 299.0 || y <= -299.0 {
            y_vel *= -1.0;
            ball.velocity = (x_vel, y_vel);
        }
        // QUESTION: Why doesn't this seem to work?
        ball.x_y = (x + x_vel, y + y_vel)
    }
}

fn view(app: &App, model: &Model, frame: &Frame) {
    let draw = app.draw();
    draw.background().color(WHITE);
    for ball in model.balls.iter() {
        let (x, y) = ball.x_y;
        draw.ellipse()
            .x_y(x, y)
            .w_h(ball.size, ball.size)
            .color(PINK);
    }

    draw.to_frame(app, &frame).unwrap();
}
