use nannou::prelude::*;
use nannou_audio as audio;
use nannou_audio::Buffer;
use nannou::ui::prelude::*;

use audio_tools::{synth};
use synth::{BasicSynth, Message, OscType};

mod virtual_keyboard;
use virtual_keyboard::{VirtualKeyboard};

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    ui: Ui,
    ui_params: UIParams,
    ids: Ids,
    keyboard: VirtualKeyboard,
    stream: audio::Stream<BasicSynth>,
    notes: Vec<Option<NoteBall>>,
}

struct Ids {
    attack: widget::Id,
    release: widget::Id,
    volume: widget::Id,
    filter_freq: widget::Id,
    table_1: widget::Id,
    table_2: widget::Id,
    table_3: widget::Id,
    table_4: widget::Id,
    delay_wetdry: widget::Id,
    delay_params: widget::Id,
}

struct UIParams {
    attack: f32,
    release: f32,
    volume: f32,
    filter_freq: f32,
    delay_wetdry: f32,
    delay_feedback_amount: f32,
    delay_seconds: f32,
}

#[derive(Clone)]
struct NoteBall(f32, f32);

fn model(app: &App) -> Model {
    // Create a window to receive key pressed events.
    app.new_window()
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    // Create the UI.
    let mut ui = app.new_ui().build().unwrap();

    // Generate some ids for our widgets.
    let ids = Ids {
        attack: ui.generate_widget_id(),
        release: ui.generate_widget_id(),
        volume: ui.generate_widget_id(),
        filter_freq: ui.generate_widget_id(),
        table_1: ui.generate_widget_id(),
        table_2: ui.generate_widget_id(),
        table_3: ui.generate_widget_id(),
        table_4: ui.generate_widget_id(),
        delay_wetdry: ui.generate_widget_id(),
        delay_params: ui.generate_widget_id(),
    };

    let ui_params = UIParams {
        attack: 0.01,
        release: 0.5,
        volume: 0.5,
        filter_freq: 0.5,
        delay_wetdry: 0.5,
        delay_feedback_amount: 0.7,
        delay_seconds: 0.25,
    };

    // Initialise the audio API so we can spawn an audio stream.
    let audio_host = audio::Host::new();
    let model: BasicSynth = BasicSynth::new();
    let stream = audio_host
        // this refers to the Audio struct, not the Model
        .new_output_stream(model)
        // This refers to the function used to process audio
        // TODO: rename audio function to process_block or something.
        .render(audio)
        .build()
        .unwrap();

    Model { 
        ui,
        ids,
        ui_params,
        notes: vec![None; 128],
        keyboard: VirtualKeyboard::new(),
        stream,
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    for note in model.notes.iter_mut() {
        match note {
            None => {}
            Some(NoteBall(_x, y)) => {
                *y += 1.0;
            }
        }
    }

    // Calling `set_widgets` allows us to instantiate some widgets.
    let ui = &mut model.ui.set_widgets();

    fn slider(val: f32, min: f32, max: f32) -> widget::Slider<'static, f32> {
        widget::Slider::new(val, min, max)
            .w_h(200.0, 30.0)
            .label_font_size(15)
            .rgb(0.3, 0.3, 0.3)
            .label_rgb(1.0, 1.0, 1.0)
            .border(0.0)
    }

    for value in slider(model.ui_params.volume as f32, 0.0, 1.0);
        .top_left_with_margin(20.0)
        .label("Volume")
        .set(model.ids.volume, ui) {
            model.ui_params.volume = value as f32;
            model
                .stream
                .send(move |synth| { synth.send(Message::SetVolume(value)); })
                .unwrap();
        }

    for value in slider(model.ui_params.attack as f32, 0.0, 1.0)
        .down(10.0)
        .label("Attack")
        .set(model.ids.attack, ui) {
            model.ui_params.attack = value as f32;
            model
                .stream
                .send(move |synth| { synth.send(Message::SetEnvAttack(value)); })
                .unwrap();
        }

    for value in slider(model.ui_params.release as f32, 0.0, 1.0)
        .down(10.0)
        .label("Release")
        .set(model.ids.release, ui) {
            model.ui_params.release = value as f32;
            model
                .stream
                .send(move |synth| { synth.send(Message::SetEnvRelease(value)); })
                .unwrap();
        }

    for value in slider(model.ui_params.filter_freq as f32, 0.0, 1.0)
        .down(10.0)
        .label("Filter Freq")
        .set(model.ids.filter_freq, ui) {
            model.ui_params.filter_freq = value as f32;
            model
                .stream
                .send(move |synth| { synth.send(Message::SetFilterFreq(value)); })
                .unwrap()
        }
    
    for _click in widget::Button::new()
        .right(50.0)
        .align_top()
        .w_h(200.0, 60.0)
        .label("Sine Osc")
        .label_font_size(15)
        .rgb(0.3, 0.3, 0.3)
        .label_rgb(1.0, 1.0, 1.0)
        .border(0.0)
        .set(model.ids.table_1, ui) {
            model
                .stream
                .send(move |synth| {
                    synth.send(Message::SetOscillator(OscType::Sine));
                })
                .unwrap();
        }

    for _click in widget::Button::new()
        .down(10.0)
        .w_h(200.0, 60.0)
        .label("Triangle Osc")
        .label_font_size(15)
        .rgb(0.3, 0.3, 0.3)
        .label_rgb(1.0, 1.0, 1.0)
        .border(0.0)
        .set(model.ids.table_2, ui) {
            model
                .stream
                .send(move |synth| {
                    synth.send(Message::SetOscillator(OscType::Triangle));
                })
                .unwrap();
        }
    for _click in widget::Button::new()
        .down(10.0)
        .w_h(200.0, 60.0)
        .label("Square Osc")
        .label_font_size(15)
        .rgb(0.3, 0.3, 0.3)
        .label_rgb(1.0, 1.0, 1.0)
        .border(0.0)
        .set(model.ids.table_3, ui) {
            model
                .stream
                .send(move |synth| {
                    synth.send(Message::SetOscillator(OscType::Square));
                })
                .unwrap();
        }

    for _click in widget::Button::new()
        .down(10.0)
        .w_h(200.0, 60.0)
        .label("Saw Table")
        .label_font_size(15)
        .rgb(0.3, 0.3, 0.3)
        .label_rgb(1.0, 1.0, 1.0)
        .border(0.0)
        .set(model.ids.table_4, ui) {
            model
                .stream
                .send(move |synth| {
                    synth.send(Message::SetOscillator(OscType::Sawtooth));
                })
                .unwrap();
        }

    for value in slider(model.ui_params.delay_wetdry as f32, 0.0, 1.0)
        .down(10.0)
        .label("Delay Wet/Dry")
        .set(model.ids.delay_wetdry, ui) {
            model.ui_params.delay_wetdry = value as f32;
            model
                .stream
                .send(move |synth| { synth.send(Message::SetDelayWetdry(value)); })
                .unwrap();
        }
    
    for (x, y) in widget::XYPad::new(
        model.ui_params.delay_seconds,
        0.001,
        1.8,
        model.ui_params.delay_feedback_amount,
        0.0,
        1.0,
    )
    .down(10.0)
    .w_h(200.0, 200.0)
    .label("X: Seconds, Y: Feedback")
    .label_font_size(15)
    .rgb(0.3, 0.3, 0.3)
    .label_rgb(1.0, 1.0, 1.0)
    .border(0.0)
    .set(model.ids.delay_params, ui) {
        model.ui_params.delay_seconds = x;
        model.ui_params.delay_feedback_amount = y;
        model
            .stream
            .send(move |synth| {
                synth.send(Message::SetDelaySeconds(x));
            })
            .unwrap();
        model
            .stream
            .send(move |synth| {
                synth.send(Message::SetDelayFeedback(y));
            })
            .unwrap();
    }
}

// A function that renders the given `BasicSynth` to the given `Buffer`.
// Most of the work is deferred to the BasicSynth module provided by audio_tools.
fn audio(synth: &mut BasicSynth, buffer: &mut Buffer) {
    let sr = buffer.sample_rate();
    for frame in buffer.frames_mut() {
        let output_sample = synth.tick(sr);
        for channel in frame {
            *channel = output_sample;
        }
    }
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match model.keyboard.process_input(key) {
        Some(instruction) => {
            match instruction {
                virtual_keyboard::Message::NoteOn(note, vel) => {
                    model
                        .stream
                        .send(move |synth| {
                            synth.send(Message::NoteOn(note, vel))
                        })
                        .unwrap();
                    model.notes[note as usize] = 
                        Some(NoteBall(note as f32 * 5.0 - 200.0, -100.0));
                }
                virtual_keyboard::Message::Pause => {
                    if model.stream.is_playing() {
                        model.stream.pause().unwrap();
                    } else {
                        model.stream.play().unwrap();
                    }
                }
            }
        }
        None => {}
    }
}

fn view(app: &App, model: &Model, frame: &Frame) {
    let draw = app.draw();
    draw.background().color(WHITE);
    for note in model.notes.iter() {
        match note {
            None => {}
            Some(NoteBall(x, y)) => {
                draw.ellipse()
                    .x_y(*x, *y)
                    .w_h(25.0, 25.0)
                    .color(PINK);
            }
        }
    }
    draw.to_frame(app, &frame).unwrap();

    // Draw the state of the `Ui` to the frame.
    model.ui.draw_to_frame(app, &frame).unwrap();
}
