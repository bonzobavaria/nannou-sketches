use nannou::prelude::*;

fn main() {
    nannou::app(model).update(update).run()
}

struct Model {
    angle: f32,
}

fn model(app: &App) -> Model {
    app.new_window()
        .with_dimensions(600, 600)
        .view(view)
        .build()
        .unwrap();
    Model { angle: 0.0 }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    model.angle += 0.1;
}

fn view(app: &App, model: &Model, frame: &Frame) {
    let draw = app.draw();
    draw.background().color(WHITE);
    draw.line()
        .start(pt2(-60.0, 0.0))
        .end(pt2(60.0, 0.0))
        .color(BLACK)
        .rotate(model.angle);

     draw.rect()
        .x_y(-100.0, 0.0)
        .w_h(20.0, 20.0)
        .hsv(1.0, 1.0, 0.5)
        .rotate(model.angle);

    // In Nannou, rotation means something rotates relative to its own center.
    // So rotating a circle does nothing. Here's the tried and true way of
    // creating circular motion.
    draw.ellipse()
        .x_y(
            model.angle.sin() * 100.0,
            model.angle.cos() * 100.0,
        )
        .w_h(25.0, 25.0)
        .color(PERU)
        .rotate(model.angle);

    draw.to_frame(app, &frame).unwrap();
}
